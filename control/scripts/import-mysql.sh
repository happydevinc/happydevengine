#!/usr/bin/env bash

DB=$1;
FILE=$2;

mysql -uhomestead -psecret -e "CREATE DATABASE IF NOT EXISTS \`$DB\` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci";
mysql -uhomestead -psecret $DB < $FILE;
