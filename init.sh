#!/usr/bin/env bash

devRoot=.dev

mkdir -p "$devRoot"

cp -i control/src/stubs/Dev.yaml "$devRoot/Dev.yaml"
cp -i control/src/stubs/after.sh "$devRoot/after.sh"
cp -i control/src/stubs/aliases "$devRoot/aliases"
cp -i control/src/stubs/Vagrantfile "."

echo "Happy Coding!"
