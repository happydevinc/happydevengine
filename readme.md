# Happydev Engine

This a rouge fork of [Laravel/Homestead](https://github.com/laravel/homestead).

## Requirements

1. Vagrant
2. Virtualbox
3. Homestead vagrant box. You can download it [here](https://atlas.hashicorp.com/laravel/boxes/homestead/versions/0.4.1/providers/virtualbox.box). Install the box by entering `vagrant box add <preferred_box_name> /path/to/file.box`. Just rename the downloaded file to `<some_cool_name>.box` if it has no `.box` extension.

## Setup

1. [Download](https://bitbucket.org/happydevinc/happydevengine/downloads) and extract. It is best to move the downloaded file to your current working directory or drive. Rename the extracted folder to **preferred_box_name**.
2. Open command line (GitBash, Terminal) and navigate to the extracted folder.
3. Enter command: `bash init.sh` *(this will copy .dev folder and Vagrantfile to the root folder)*
4. Edit configurations by opening Dev.yaml located at .dev folder. **Important:** *change the hostname key value to the **preferred_box_name** from the requirements (see requirements item 3)*
5. Fire the setup by entering `vagrant up`
6. To access the instance, open your favorite browser (Internet Explorer, if you know what I mean :P) and enter  `http://<ip_from_the_config_file>`.


## Configuring `vhost`
Configure vhost by adding the `<ip_from_the_config_file>` to your host file and add `<sites_to_key_value_from_the_config_file>` as its map. It should look like the one below:

```
<ip_from_the_config_file>     <sites_to_key_value_from_the_config_file>

192.168.20.20   local-dev.app
```
It should be noted that this process needs the instance to be rebooted or reloaded. To reboot/reload the instance, enter `vagrant reload --provision` or `vagrant up`.


## Useful Commands

To start instance, use `vagrant up`. Use `vagrant halt` or `vagrant suspend` to safely save all changes made to the instance (file edits from your sites, db updates). Only use `vagrant destroy` if you know what you're doing, this command will **destroy** everything. Check [here](https://docs.vagrantup.com/v2/cli/index.html) for more of the vagrant commands.